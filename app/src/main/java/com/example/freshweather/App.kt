package com.example.freshweather

import android.app.Application
import com.example.freshweather.koin.globalModule
import org.koin.android.ext.android.startKoin

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(globalModule))
    }
}