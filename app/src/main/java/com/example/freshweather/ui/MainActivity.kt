package com.example.freshweather.ui

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.freshweather.R
import com.example.freshweather.data.WeatherAppData
import com.example.freshweather.repository.WeatherNetworkRepository

class MainActivity : AppCompatActivity() {

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val weather = WeatherNetworkRepository().getWeather("minsk")
        weather.subscribe {info(it)}
    }

    private fun info(weatherAppData: WeatherAppData?) {
        weatherAppData?.country
    }
}
