package com.example.freshweather.koin

import com.example.freshweather.data.network.NetworkClient
import org.koin.dsl.module.module

val globalModule = module {
    single { NetworkClient() }
}