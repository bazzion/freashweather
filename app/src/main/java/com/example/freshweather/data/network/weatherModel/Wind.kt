package com.example.freshweather.data.network.weatherModel

data class Wind(
    var speed: Double? = null,
    var deg: Double? = null
) {}