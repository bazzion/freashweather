package com.example.freshweather.data.network

import com.example.freshweather.data.network.weatherModel.WeatherResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface WeatherApi {
    @GET("data/2.5/weather?APPID=6a230e4949c456d8e49596266657dbaf&units=metric&lang=ru")
    fun getCurrentWeatherData(@Query("q") city: String): Observable<WeatherResponse>
}