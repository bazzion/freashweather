package com.example.freshweather.data

data class WeatherAppData(
    var country: String? = null,
    var sunrise: Int? = null,
    var sunset: Int? = null,
    var weather: String? = null,
    var humidity: Int? = null,
    var pressure: Double? = null,
    var temperature: Double? = null
) {}