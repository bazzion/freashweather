package com.example.freshweather.data.network.weatherModel

data class Main(
    var temp: Double? = null,
    var pressure: Double? = null,
    var humidity: Int? = null,
    var tempMin: Double? = null,
    var tempMax: Double? = null
) {}
