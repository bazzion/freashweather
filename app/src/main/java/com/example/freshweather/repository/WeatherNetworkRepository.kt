package com.example.freshweather.repository

import com.example.freshweather.data.WeatherAppData
import com.example.freshweather.data.network.NetworkClient
import com.example.freshweather.data.network.WeatherApi
import com.example.freshweather.data.network.weatherModel.WeatherResponse
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class WeatherNetworkRepository: KoinComponent {

    private val client: NetworkClient by inject()

    fun getWeather(city: String): Observable<WeatherAppData> {
        return getData(city)
            .map { getItems(it) }
            .subscribeOn(Schedulers.io())
    }

    private fun getItems(list: WeatherResponse): WeatherAppData = WeatherAppData(
        list.sys!!.country,
        list.sys!!.sunrise,
        list.sys!!.sunset,
        list.weather!![0].main,
        list.main!!.humidity,
        list.main!!.pressure,
        list.main!!.temp
        )

    private fun getData(city: String): Observable<WeatherResponse> =
        client.getClient(WeatherApi::class.java, client.baseUrl).getCurrentWeatherData(city)

}